tool
extends Area

class_name GravArea

export(int) var gravPriority = 0;
export(Vector3) var gravDirection = Vector3.DOWN setget setGravDirection;
export(float) var gravPower = 9.8;

var trueGravity;

func _ready():
	$MeshInstance.material_override = $MeshInstance.material_override.duplicate()
	setGravDirection(gravDirection)

func _physics_process(delta):
	pass

const bodies = {}
func _on_GravArea_body_entered(body:Spatial):
	if body.has_method("setGrav"):
		if not bodies.has(body):
			bodies[body] = {}
		bodies[body][self] = self
		recalcGrav(body)

func _on_GravArea_body_exited(body:Spatial):
	if body.has_method("setGrav"):
		if not bodies.has(body):
			bodies[body] = {}
		bodies[body].erase(self)
		recalcGrav(body)

static func recalcGrav(body:Spatial):
	var prio = -INF
	var grav:Vector3= Vector3.ZERO;
	for area in bodies[body]:
		if area.gravPriority > prio:
			prio = area.gravPriority
			grav = area.gravDirection * area.gravPower
			pass #replace grav
		elif area.gravPriority == prio:
			grav += area.gravDirection * area.gravPower
			pass #add to grav
	body.setGrav(grav)

func setGravDirection(value):
	gravDirection = value
	if gravDirection == Vector3.ZERO:
		value = Vector3.DOWN
	if has_node("MeshInstance"):
		$MeshInstance.material_override.set_shader_param("GravDir",-value)
