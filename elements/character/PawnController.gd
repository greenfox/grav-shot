extends KinematicBody

class_name PawnController

var grav = Vector3.ZERO setget setGrav
var velocity = Vector3.ZERO 

export(float) var jetpackPower = 10

export(NodePath) var cameraPath = null

signal processStateMachine(pawn,delta)

var input:InputContainer

var pitchAngle = 0

func _physics_process(delta):
	input = $InputManager.getInput()
	emit_signal("processStateMachine",self,delta)
#	$ControlStates._processInput($InputManager.getInput(),delta)
	
func _process(delta):
	if cameraPath != null:
		get_node(cameraPath).global_transform = $cameraGimble.global_transform
	


func requestJetpackeFuel(requestedFuel:float)->float:
	return requestedFuel


func setGrav(newGrav:Vector3):
	grav = newGrav
