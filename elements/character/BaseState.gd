extends Node

class_name BaseState

signal changeState(pawn,name)

func _processState(pawn:PawnController,delta:float):
	assert(false,"Every state MUST overload _processState!")

func _checkStateChange(pawn:PawnController):
	assert(false,"Every state MUST overload _checkStateChange!")

func _leaveState(pawn:PawnController,newState:String):
	pass
	
func _enterState(pawn:PawnController,oldState:String):
	pass
