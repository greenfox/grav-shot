extends Node

class_name PlayerControls

var inputContainer:= InputContainer.new()

var mouseDelta:=Vector2.ZERO

func getInput()->InputContainer:
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		return inputContainer.reset()
	var inputVector = Vector3.ZERO
	inputContainer.spin = 0
	
	if Input.is_action_pressed("forward"):		inputVector += Vector3.FORWARD
	if Input.is_action_pressed("backward"):		inputVector += Vector3.BACK
	if Input.is_action_pressed("left"):			inputVector += Vector3.LEFT
	if Input.is_action_pressed("right"):		inputVector += Vector3.RIGHT
	if Input.is_action_pressed("up"):			inputVector += Vector3.UP
	if Input.is_action_pressed("down"):			inputVector += Vector3.DOWN
	if Input.is_action_pressed("spinLeft"):		inputContainer.spin += 1
	if Input.is_action_pressed("spinRight"):	inputContainer.spin -= 1
	inputContainer.direction = inputVector
	inputContainer.look = mouseDelta
	mouseDelta = Vector2.ZERO

	inputContainer.breaks = Input.is_action_pressed("breaks")

	return inputContainer


func _unhandled_input(event):
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event is InputEventMouseMotion:
			mouseDelta += event.relative
