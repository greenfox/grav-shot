extends Node

class_name ControlStates

onready var currentState :BaseState= $Flying

func _on_processStateMachine(pawn:PawnController,delta:float):
	currentState._checkStateChange(pawn)
	currentState._processState(pawn,delta)


func _on_changeState(pawn:PawnController,name:String):
	var oldState = currentState.name
	print("stateChange:" , oldState , "->", name)
	currentState._leaveState(pawn,name)
	currentState = get_node(name)
	currentState._enterState(pawn,oldState)

