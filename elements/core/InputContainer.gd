extends Resource

class_name InputContainer

var direction:Vector3 = Vector3.ZERO setget updateDirection
var normalDirection:Vector3 = Vector3.ZERO 

var look:Vector2 = Vector2.ZERO
var jump:bool = false
var spin:float = 0.0

var breaks:bool = false

func updateDirection(value):
	direction = value
	normalDirection = direction.normalized()
	

func reset():
	updateDirection(Vector3.ZERO)
	look = Vector2.ZERO
	jump = false
	spin = 0
	return self
