extends CanvasLayer

export(NodePath) var Player

export(bool) var overrideHide = false

var defaultProps:={}

func _ready():
	call_deferred("setDefaultProps")

func _process(delta):
	if Input.is_action_just_pressed("esc") or not OS.is_window_focused():
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		$CenterContainer.visible = true

	if Player != null:
		var v:Vector3 = get_node(Player).velocity
		$MarginContainer/Label.text = String(v.length()) + " " + String(v) + "\n"
		$MarginContainer/Label.text += String(get_node(Player).grav)
	
	if overrideHide:
		$CenterContainer.visible = false

func _on_Button_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$CenterContainer.visible = false


func setDefaultProps():
	defaultProps.transform = get_node(Player).global_transform
	defaultProps.velocity = get_node(Player).velocity


func _on_reset_pressed():
	get_node(Player).global_transform = defaultProps.transform
	get_node(Player).velocity = defaultProps.velocity
