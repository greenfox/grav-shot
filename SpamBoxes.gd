extends Spatial

export var count:int = 100;
export var area:float = 100;

var boxes = []

func _ready():
	call_deferred("initBoxes")
	

func initBoxes():
	boxes.resize(count)
	for i in count:
		var newBox = preload("res://box.tscn").instance()
		add_child(newBox)
		newBox.translation = (Vector3(randf(),randf(),randf()) - (Vector3(0.5,0,0.5))) * area
		boxes[i] = newBox

#func reposition():
#	for box in boxes:
#		var t:Transform= 
#		var randV:Vector3= Vector3(randf(),randf(),randf()) - (Vector3.ONE * 0.5) #this will prefer conners. don't know if I care
